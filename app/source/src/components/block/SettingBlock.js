import React from 'react';
import UserContext from "../../entity/UserContext";
import {requestWithAuthCheck} from "../../functions/RequestFunctions";
import {NotificationInfo} from "../../functions/NotificationFunctions";

export default class SettingBlock extends React.Component {
    static contextType = UserContext;

    constructor(props) {
        super(props);

        this.saveSetting = this.saveSetting.bind(this);
    }

    saveSetting() {
        requestWithAuthCheck(
            '/api/v1/setting',
            'PUT',
            this.context.state.setting,
            {
                'Authorization': 'Bearer ' + this.context.state.token
            }
        ).then((response) => {
            NotificationInfo('Success save')
        });
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="inputGroup-sizing-sm">Binance Public Key</span>
                            </div>

                            <input value={this.context.state.setting.binance_public_key || ''}
                                   onChange={this.context.setBinancePublicKey}
                                   type="text"
                                   className="form-control"/>
                        </div>

                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="inputGroup-sizing-sm">Binance Private Key</span>
                            </div>

                            <input value={this.context.state.setting.binance_private_key || ''}
                                   onChange={this.context.setBinancePrivateKey}
                                   type="text"
                                   className="form-control"/>
                        </div>

                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="inputGroup-sizing-sm">MonoBank Token</span>
                            </div>

                            <input value={this.context.state.setting.mono_bank_token || ''}
                                   onChange={this.context.setMonoBankToken}
                                   type="text"
                                   className="form-control"/>

                            <a style={styles.link} target="_blank" href="https://api.monobank.ua/">Token</a>
                        </div>

                        <div className="form-check">
                            <fieldset className="scheduler-border">
                                <legend className="scheduler-border">CoinPriceCheck algorithm</legend>

                                <div className="form-check form-switch">
                                    <input className="form-check-input"
                                           type="checkbox"
                                           name="notification"
                                           data-algorithm="coin_price_spot"
                                           checked={this.context.state.setting.algorithm_settings.coin_price_spot.notification || false}
                                           onChange={this.context.setAlgorithmSetting}/>
                                    <label className="form-check-label">Enable notification</label>
                                </div>

                                <div className="form-check form-switch">
                                    <input className="form-check-input"
                                           type="checkbox"
                                           name="real_buy"
                                           data-algorithm="coin_price_spot"
                                           checked={this.context.state.setting.algorithm_settings.coin_price_spot.real_buy || false}
                                           onChange={this.context.setAlgorithmSetting}/>
                                    <label className="form-check-label">Enable <b>real transaction</b></label>
                                </div>
                            </fieldset>
                        </div>

                        <div className="form-check">
                            <fieldset className="scheduler-border">
                                <legend className="scheduler-border">TrendChange algorithm</legend>

                                <div className="form-check form-switch">
                                    <input className="form-check-input"
                                           type="checkbox"
                                           name="notification"
                                           data-algorithm="trend_change"
                                           checked={this.context.state.setting.algorithm_settings.trend_change.notification || false}
                                           onChange={this.context.setAlgorithmSetting}/>
                                    <label className="form-check-label">Enable notification</label>
                                </div>

                                <div className="form-check form-switch">
                                    <input className="form-check-input"
                                           type="checkbox"
                                           name="real_buy"
                                           data-algorithm="trend_change"
                                           checked={this.context.state.setting.algorithm_settings.trend_change.real_buy || false}
                                           onChange={this.context.setAlgorithmSetting}/>
                                    <label className="form-check-label">Enable <b>real transaction</b></label>
                                </div>
                            </fieldset>
                        </div>

                        <div className="input-group">
                            <a style={styles.link} target="_blank" href="https://t.me/finmon_kava_bot">Telegram bot</a>
                        </div>

                        <button onClick={this.saveSetting}
                                className="btn margin-around btn-primary"
                                type="button">Save
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

const styles = {
    link: {
        marginTop: "5px",
        marginLeft: "15px",
    }
}
