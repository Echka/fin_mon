import React from 'react';

export default class TableBlock extends React.Component {
    getDataByRecursive(item, fieldArray) {
        var data = item;

        fieldArray.map((field) => {
            data = data[field];
        });

        return data;
    }

    getDataByField(item, field) {
        if (Array.isArray(field)) {
            return this.getDataByRecursive(item, field);
        }

        return item[field];
    }

    render() {
        return (
            <div className="card">
                <div className="card-body table-responsive p-0">
                    <table className="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            {this.props.headers.map((header, index) => {
                                return (
                                    <th scope="col" key={index}>{header}</th>
                                )
                            })}
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.data.map((item, index) => {
                            return (
                                <tr className={item.classRow} key={index}>
                                    {this.props.dataFields.map((field, index) => {
                                        return (
                                            <td key={index}>{this.getDataByField(item, field)}</td>
                                        );
                                    })}
                                </tr>
                            );
                        })}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    };
}
