import React from 'react';
import UserContext from "../../entity/UserContext";
import {requestWithAuthCheck} from "../../functions/RequestFunctions";
import TableBlock from "./TableBlock";
import moment from 'moment';
import {round} from "../../functions/CommonFunctions";

export default class OrderOperationBlock extends React.Component {
    static contextType = UserContext;

    resultTypesClass = {
        "-1": 'fail',
        "0": '',
        "1": 'success',
    };

    constructor(props) {
        super(props);

        this.state = {
            orderPositions: [],
            dataFields: [
                'algorithm_name',
                'coin_name',
                'type',
                'leverage',
                'market_price',
                'close_price',
                'close_percent',
                'profit_percent',
                'date'
            ],
            headers: [
                'Algorithm',
                'Coin',
                'Type',
                'Leverage',
                'Price',
                'Close price',
                'Close percent',
                'Profit percent',
                'Date'
            ],
            autoSelectFilters: [
                'algorithm_name',
                'coin_name',
                'type',
            ]
        };
    }

    componentDidMount() {
        this.setOrderPositions()
    }

    setOrderPositions() {
        requestWithAuthCheck(
            '/api/v1/order_position',
            'GET',
            {},
            {
                'Authorization': 'Bearer ' + this.context.state.token
            }
        ).then(response => {
            response.result.map((orderPosition) => {
                orderPosition.classRow = this.resultTypesClass[orderPosition.result];
                orderPosition.coin_name = orderPosition.coin.name;
                orderPosition.date = moment(orderPosition.date).format('YYYY-MM-DD hh:mm:ss');
                orderPosition.close_price = round(orderPosition.close_price);
                orderPosition.close_percent = round(orderPosition.close_percent);
            });

            this.setState({
                orderPositions: response.result
            });
        });
    }

    render() {
        return (
            <div>
                <TableBlock headers={this.state.headers} dataFields={this.state.dataFields}
                            data={this.state.orderPositions} autoSelectFilters={this.state.autoSelectFilters}/>
            </div>
        );
    }
}
