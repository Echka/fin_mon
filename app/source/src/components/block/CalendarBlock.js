import React from 'react';

export default class CalendarBlock extends React.Component {
    getDataRows(dataObject) {
        let rows = [],
            emptyRow = [null, null, null, null, null, null, null],
            currentRow = [...emptyRow];

        dataObject.map(function (item) {
            if (item.newLine === true) {
                rows.push(currentRow);
                currentRow = [...emptyRow];
            }

            let dayOfWeek = item.date.day();

            dayOfWeek = dayOfWeek === 0 ? 6 : dayOfWeek - 1;

            currentRow[dayOfWeek] = item;
        });

        rows.push(currentRow);

        return rows;
    }

    checkAmountForDay(amountForDay) {
        return amountForDay <= this.props.avgForDay;
    }

    render() {
        return (
            <div>
                {this.getDataRows(this.props.rangeOfDays).map((row, index) => {
                    var sum = 0;

                    return (
                        <ul key={index} style={styles.ul}>
                            {row.map((item, index) => {
                                if (item === null) {
                                    return (
                                        <li key={index} style={styles.liItem}></li>
                                    );
                                }

                                let amountForDay = this.props.getAmountForDate(item.date.format('DD-MM-YYYY'));
                                sum += amountForDay;

                                return (
                                    <li key={index} style={{...styles.liItem, ...styles.liItemFilled}}>
                                        <div style={styles.divDate}>{item.date.format('DD.MM')}</div>
                                        <div style={{
                                            ...styles.divAmount,
                                            ...this.checkAmountForDay(amountForDay) ? styles.divAmountPositive : styles.divAmountNegative
                                        }}
                                             data-date={item.date.format('DD-MM-YYYY')}
                                             onClick={this.props.selectDay}>{amountForDay}</div>
                                    </li>
                                );
                            })}

                            <li style={styles.liAvg}>
                                <div>
                                    <span style={styles.liAvgValue}>{(sum / 7).toFixed(2)}</span>
                                </div>
                            </li>
                        </ul>
                    );
                })}
            </div>
        )
    };
}

const styles = {
    ul: {
        fontSize: "0.8rem",
        display: "table",
        tableLayout: "fixed",
        borderCollapse: "collapse",
        width: "100%",
        marginBottom: 0,
    },
    liItem: {
        display: "table-cell",
        boxSizing: "border-box",
        border: "1px solid #b9b9b9",
    },
    liAvg: {
        width: '10%',
        display: "table-cell",
        boxSizing: "border-box",
        listStyle: 'none',
        verticalAlign: 'bottom',
        padding: '10px',
        background: 'linear-gradient(90deg, rgba(13,176,62,0.849) 0%, rgba(134,223,146,0.675) 49%, rgba(0,255,42,0) 100%)',
        border: 'none'
    },
    liAvgValue: {
        fontSize: "0.8vw",
        padding: "0.5vw",
        fontWeight: 'bold'
    },
    liItemFilled: {
        padding: "0.4vw",
    },
    divDate: {
        fontSize: "0.8vw",
        color: "#575656"
    },
    divAmount: {
        textAlign: 'right',
        fontWeight: 'bold',
        fontSize: "1.1vw",
        marginTop: "10px",
        cursor: "pointer"
    },
    divAmountPositive: {
        color: "#70c769",
    },
    divAmountNegative: {
        color: "#c76969",
    }
};
