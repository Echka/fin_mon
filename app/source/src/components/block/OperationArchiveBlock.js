import React from 'react';
import UserContext from "../../entity/UserContext";
import {requestWithAuthCheck} from "../../functions/RequestFunctions";
import TableBlock from "./TableBlock";
import moment from 'moment';
import {NotificationInfo} from "../../functions/NotificationFunctions";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

export default class OperationArchiveBlock extends React.Component {
    static contextType = UserContext;

    constructor(props) {
        super(props);

        this.state = {
            operations: [],
            dataFields: [
                'amount',
                'description',
                'externalCode',
                'date',
                'action'
            ],
            headers: [
                'Amount',
                'Description',
                'External Code',
                'Date',
                'Action'
            ],
        };
    }

    componentDidMount() {
        this.setOperations()
    }

    getArchiveButton(operation) {
        return <FontAwesomeIcon
            style={styles.unarchiveI}
            icon="fa-box-archive"
            onClick={this.setUnarchive.bind(this)}
            data-id={operation.id}/>;
    }

    setOperations() {
        requestWithAuthCheck(
            '/api/v1/operations',
            'GET',
            {
                filters: {
                    archive: true
                }
            },
            {
                'Authorization': 'Bearer ' + this.context.state.token
            }
        ).then(response => {
            response.result.map((operation) => {
                operation.date = moment(operation.date).format('YYYY-MM-DD hh:mm:ss');
                operation.action = this.getArchiveButton(operation);
            });

            this.setState({
                operations: response.result
            });
        });
    }

    setUnarchive(event) {
        requestWithAuthCheck(
            '/api/v1/operations/archive',
            'POST',
            {
                id: parseInt(event.currentTarget.getAttribute('data-id')),
                archive: false
            },
            {
                'Authorization': 'Bearer ' + this.context.state.token
            }
        ).then(response => {
            NotificationInfo('Unarchived')

            this.setOperations()
        })
    }

    render() {
        return (
            <div>
                <TableBlock headers={this.state.headers} dataFields={this.state.dataFields}
                            data={this.state.operations}/>
            </div>
        );
    }
}

const styles = {
    unarchiveI: {
        color: "#5b963f",
        marginRight: "10px",
        cursor: "pointer"
    }
};
