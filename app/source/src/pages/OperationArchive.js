import React from 'react';
import OperationArchiveBlock from "../components/block/OperationArchiveBlock";

export default class OperationArchivePage extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <OperationArchiveBlock/>
            </div>
        );
    }
}
