const path = require("path");
const webpack = require('webpack');

module.exports = {
    mode: "production",
    entry: path.resolve(__dirname, "index.js"),
    output: {
        path: path.resolve(__dirname, "public"),
        filename: "main.js"
    },
    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    },
    devServer: {
        port: "9300",
        static: path.resolve(__dirname, "public"),
        open: true,
        compress: true,
        allowedHosts: [
            'finmon.online',
        ],
        historyApiFallback: {
            index: 'index.html'
        }
    },
    module: {
        rules: [
            {
                test: /\.m?(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
        ]
    },
    resolve: {
        modules: ['node_modules'],
        extensions: [".js", ".json", ".jsx", ".css"]
    },
    plugins: [
        new webpack.DefinePlugin({
        })
    ],
}
