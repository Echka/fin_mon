import ReactDOM from 'react-dom/client';
import React from 'react';
import {BrowserRouter} from "react-router-dom";
import {ToastContainer} from 'react-toastify';

import './node_modules/admin-lte/dist/css/adminlte.css';
import './src/css/index.css';
import './src/css/App.css'

import './node_modules/admin-lte/dist/js/adminlte.js';

import RouteList from "./src/RouteList";
import User from "./src/entity/User";

import {library} from "@fortawesome/fontawesome-svg-core";
import {
    faUser,
    faLock,
    faBars,
    faAngleLeft,
    faCircle,
    faGear,
    faWallet,
    faChartLine,
    faMugSaucer,
    faBoxArchive
} from "@fortawesome/free-solid-svg-icons";


library.add(faUser, faLock, faBars, faAngleLeft, faCircle, faGear, faWallet, faChartLine, faMugSaucer, faBoxArchive);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <BrowserRouter>
        <User>
            <RouteList/>

            <ToastContainer/>
        </User>
    </BrowserRouter>
);
