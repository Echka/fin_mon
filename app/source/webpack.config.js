const path = require("path");
const webpack = require('webpack');

module.exports = {
    mode: "development",
    entry: path.resolve(__dirname, "index.js"),
    output: {
        path: path.resolve(__dirname, "public"),
        filename: "main.js"
    },
    performance: {
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    },
    devServer: {
        port: "9300",
        static: path.resolve(__dirname, "public"),
        open: true,
        hot: true,
        liveReload: true,
        allowedHosts: [
            'finkava.com',
        ],
        historyApiFallback: {
            index: 'index.html'
        }
    },
    module: {
        rules: [
            {
                test: /\.m?(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
        ]
    },
    resolve: {
        modules: ['node_modules'],
        extensions: [".js", ".json", ".jsx", ".css"]
    },
    plugins: [
        new webpack.DefinePlugin({
        })
    ],
}
