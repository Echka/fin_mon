***For install project execute next:***

> For PHPUnit:\
> ```cp -i phpunit.xml.dist phpunit.xml```

> For Auth (generate new keys):\
> ```php bin/console lexik:jwt:generate-keypair```
>
> or
>
> mkdir config/jwt   
> ```openssl genrsa -out config/jwt/private.pem 4096``` \
> ```openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem```

> Generate migration  
> php bin/console doctrine:migrations:diff

> Run migrate
> php bin/console doctrine:migrations:migrate

> Set operations from monobank   
> bin/console mono-bank:operations:setting

> Fill coin list from Binance  
> bin/console binance:coin:list:fill

> Store permanent coin price from Binance  
> bin/console binance:store:permanent:coin_price

> Update temporary coin price from Binance  
> bin/console binance:update:temporary:coin_price

> Run Trend change algorithm
> bin/console algorithm_check:trend_change

> crontab
> *
*
*
*
* docker exec fin_mon-api-1 bin/console binance:store:permanent:coin_price >> /dev/null 2>&1
> *
*
*
*
* docker exec fin_mon-api-1 bin/console binance:update:temporary:coin_price >> /dev/null 2>&1
> *
*
*
*
* docker exec fin_mon-api-1 bin/console algorithm_check:trend_change >> /dev/null 2>&1

