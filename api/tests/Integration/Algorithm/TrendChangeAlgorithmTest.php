<?php

namespace App\tests\Integration\Algorithm;

use App\Application\Algorithm\TrendChangeAlgorithm;
use App\Domain\Contract\Factory\OrderPositionFactoryInterface;
use App\Domain\Contract\Repository\MySQL\CoinPriceRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\CoinRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\OrderPositionRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\UserRepositoryInterface;
use App\Domain\Contract\Repository\Redis\MainCryptoTrendRepositoryInterface;
use App\Domain\Contract\Repository\Redis\SpotCoinPriceRepositoryInterface;
use App\Domain\Contract\Service\Notification\NotificationServiceInterface;
use App\Domain\Entity\Coin;
use App\Domain\Entity\CoinPrice;
use App\Domain\Entity\DTO\TrendChange;
use App\Domain\Enum\OrderPositionTypeEnum;
use DateTime;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

class TrendChangeAlgorithmTest extends TestCase
{
    private TrendChangeAlgorithm $algorithm;
    private MockObject $coinPriceRepository;
    private MockObject $coinRepository;
    private MockObject $orderPositionFactory;
    private MockObject $orderPositionRepository;
    private MockObject $spotCoinPriceRepository;
    private MockObject $mainCryptoTrendRepository;

    public function testCheckTrendByCoinReturnsValidShortTrend(): void
    {
        // Arrange
        $coin = (new Coin())
            ->setId(1)
            ->setName('BTC')
            ->setFullName('Bitcoin');
        $coinPriceData = [
            40835.93,
            39536.44,
            39804.56,
            39523.60,
            40159.63,
            38089.13,
            39150.38,
            39974.36,
            38555.20,
            38355.19,
            37939.74,
            38386.61,
            37622.31,
            39825.60,
            36314.56,
            36038.00,
            35771.41,
            34357.72,
            30918.35,
            31113.83,
            29165.87,
            28765.63,
            29871.72,
            29361.83,
            30920.84,
            29785.55,
            30179.22,
            29128.39,
            30124.93,
            29159.48,
            29390.11,
            30007.67,
            29401.46,
            29391.05,
            29707.39,
            29494.72,
            28835.51,
            28988.19,
            29142.98,
            31195.35,
            31690.76,
            29585.11,
            30286.22,
            29653.52,
            29721.45,
            29993.37,
            31373.97,
            31338.31,
            30222.22,
            30121.28,
            29171.12,
            28494.96,
            27422.44,
            23426.06,
            21912.07,
            21825.12,
            20678.86,
            20641.79,
            17764.77,
            20585.68,
            20277.13,
            20871.35,
            19915.95,
            20913.79,
            21182.53,
            21224.30,
            21346.52,
            20914.18,
            20273.29,
            20156.41,
            18849.17,
            19506.58,
            19250.40,
            19430.39,
            19777.26,
            20487.14,
            20308.33,
            21582.47,
            21848.77,
            21651.22,
            20945.76,
            20459.29,
            19436.56,
            19657.33,
            20598.36,
            20948.30,
            21124.31,
            20945.86,
            21547.82,
            23227.27,
            23219.76,
            23199.57,
            22605.88,
            22293.40,
            22764.72,
            22140.63,
            20919.35,
            22790.25,
            23867.02,
            23953.86,
            23978.44,
            23815.01,
            23028.10,
            23002.07,
            23325.36,
            22541.80,
            22953.87,
            23142.95,
            23238.29,
            23970.69,
            23134.32,
            23762.90,
            24201.57,
            24121.91,
            24472.55,
            24318.14,
            24023.08,
            23930.77,
            23398.95,
            23414.68,
            21259.00,
            21046.90,
            21516.94,
            21141.69,
            21489.97,
            21718.35,
            21653.28,
            20695.03,
            19976.71,
            20014.45,
            20174.09,
            20036.76,
            20186.38,
            20025.29,
            19935.20,
            19737.48,
            19900.14,
            19770.90,
            18938.30,
            19344.57,
            19378.28,
            21276.02,
            21413.10,
            21537.77,
            22426.66,
            20340.42,
        ];
        $spotPrice = '19964.12';

        $coinPriceData = array_map(
            fn(float $price) => (new CoinPrice())
                ->setMarketPrice($price)
                ->setCoin($coin)
                ->setDate(new DateTime()),
            $coinPriceData,
        );

        $this->coinPriceRepository->expects($this->once())
            ->method('getLastByHours')
            ->with(self::anything(), $coin)
            ->willReturn($coinPriceData);

        $this->spotCoinPriceRepository->expects($this->once())
            ->method('find')
            ->with($coin->getPairName())
            ->willReturn($spotPrice);

        // Reflection for the private method
        $refMethod = new ReflectionMethod(TrendChangeAlgorithm::class, 'checkTrendByCoin');

        // Act
        /* @var TrendChange $result */
        $result = $refMethod->invoke($this->algorithm, $coin);

        // Assert
        $this->assertEquals(OrderPositionTypeEnum::SHORT_TYPE, $result->getTrend());
    }

    public function testCheckTrendByCoinReturnsValidLongTrend(): void
    {
        // Arrange
        $coin = (new Coin())
            ->setId(1)
            ->setName('BTC')
            ->setFullName('Bitcoin');
        $coinPriceData = [
            40835.93,
            39536.44,
            39804.56,
            39523.60,
            40159.63,
            38089.13,
            39150.38,
            39974.36,
            38555.20,
            38355.19,
            37939.74,
            38386.61,
            37622.31,
            39825.60,
            36314.56,
            36038.00,
            35771.41,
            34357.72,
            30918.35,
            31113.83,
            29165.87,
            28765.63,
            29871.72,
            29361.83,
            30920.84,
            29785.55,
            30179.22,
            29128.39,
            30124.93,
            29159.48,
            29390.11,
            30007.67,
            29401.46,
            29391.05,
            29707.39,
            29494.72,
            28835.51,
            28988.19,
            29142.98,
            31195.35,
            31690.76,
            29585.11,
            30286.22,
            29653.52,
            29721.45,
            29993.37,
            31373.97,
            31338.31,
            30222.22,
            30121.28,
            29171.12,
            28494.96,
            27422.44,
            23426.06,
            21912.07,
            21825.12,
            20678.86,
            20641.79,
            17764.77,
            20585.68,
            20277.13,
            20871.35,
            19915.95,
            20913.79,
            21182.53,
            21224.30,
            21346.52,
            20914.18,
            20273.29,
            20156.41,
            18849.17,
            19506.58,
            19250.40,
            19430.39,
            19777.26,
            20487.14,
            20308.33,
            21582.47,
            21848.77,
            21651.22,
            20945.76,
            20459.29,
            19436.56,
            19657.33,
        ];
        $spotPrice = '20598.36';

        $coinPriceData = array_map(
            fn(float $price) => (new CoinPrice())
                ->setMarketPrice($price)
                ->setCoin($coin)
                ->setDate(new DateTime()),
            $coinPriceData,
        );

        $this->coinPriceRepository->expects($this->once())
            ->method('getLastByHours')
            ->with(self::anything(), $coin)
            ->willReturn($coinPriceData);

        $this->spotCoinPriceRepository->expects($this->once())
            ->method('find')
            ->with($coin->getPairName())
            ->willReturn($spotPrice);

        // Reflection for the private method
        $refMethod = new ReflectionMethod(TrendChangeAlgorithm::class, 'checkTrendByCoin');

        // Act
        /* @var TrendChange $result */
        $result = $refMethod->invoke($this->algorithm, $coin);

        // Assert
        $this->assertEquals(OrderPositionTypeEnum::LONG_TYPE, $result->getTrend());
    }

    protected function setUp(): void
    {
        $this->coinPriceRepository = $this->createMock(CoinPriceRepositoryInterface::class);
        $this->spotCoinPriceRepository = $this->createMock(SpotCoinPriceRepositoryInterface::class);
        $this->orderPositionRepository = $this->createMock(OrderPositionRepositoryInterface::class);
        $this->coinRepository = $this->createMock(CoinRepositoryInterface::class);
        $this->mainCryptoTrendRepository = $this->createMock(MainCryptoTrendRepositoryInterface::class);
        $this->orderPositionFactory = $this->createMock(OrderPositionFactoryInterface::class);

        $this->algorithm = new TrendChangeAlgorithm(
            $this->coinPriceRepository,
            $this->orderPositionRepository,
            $this->coinRepository,
            $this->orderPositionFactory,
            $this->spotCoinPriceRepository,
            $this->createMock(UserRepositoryInterface::class),
            $this->createMock(NotificationServiceInterface::class),
            $this->mainCryptoTrendRepository,
        );
    }
}
