<?php

namespace App\Infrastructure\Client;

use App\Domain\Entity\DTO\BinanceToken;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class BinanceClient
{
    private const SPOT_ACCOUNT_URL = 'api/v3/account';
    private const MARGIN_ACCOUNT_URL = 'sapi/v1/margin/account';
    private const MY_TRADES_SPOT_URL = 'api/v3/myTrades';
    private const MY_TRADES_MARGIN_URL = 'sapi/v1/margin/myTrades';
    private const COIN_PRICE_URL = 'api/v3/ticker/price';
    private const COIN_LIST_URL = 'sapi/v1/margin/allAssets';

    public function __construct(
        private ClientInterface $client,
        private string $binanceAPIUrl,
        private string $binanceAPIKey,
    ) {
    }

    /**
     * @throws GuzzleException
     */
    public function getSpotAccountData(BinanceToken $token): ResponseInterface
    {
        return $this->requestWithTokenGET(
            url: self::SPOT_ACCOUNT_URL,
            token: $token,
        );
    }

    /**
     * @throws GuzzleException
     */
    public function getMarginAccountData(BinanceToken $token): ResponseInterface
    {
        return $this->requestWithTokenGET(
            url: self::MARGIN_ACCOUNT_URL,
            token: $token,
        );
    }

    /**
     * @throws GuzzleException
     */
    public function getMyTradesSpot(BinanceToken $token): ResponseInterface
    {
        return $this->requestWithTokenGET(
            url: self::MY_TRADES_SPOT_URL,
            token: $token,
        );
    }

    /**
     * @throws GuzzleException
     */
    public function getMyTradesMargin(BinanceToken $token): ResponseInterface
    {
        return $this->requestWithTokenGET(
            url: self::MY_TRADES_MARGIN_URL,
            token: $token,
        );
    }

    /**
     * @throws GuzzleException
     */
    public function getCoinPrice(array $coinNames): ResponseInterface
    {
        return $this->requestGET(
            url: self::COIN_PRICE_URL,
            params: [
                'symbols' => '["' . implode('","', $coinNames) . '"]',
            ],
        );
    }

    /**
     * @throws GuzzleException
     */
    public function getCoinList(): ResponseInterface
    {
        return $this->requestGET(
            url: self::COIN_LIST_URL
        );
    }

    /**
     * @throws GuzzleException
     */
    private function requestWithTokenGET(string $url, BinanceToken $token): ResponseInterface
    {
        return $this->client->request(
            method: 'GET',
            uri: $this->binanceAPIUrl . $url . '?' . $token->getParamsHttpWithToken(),
            options: [
                'headers' => [
                    'X-MBX-APIKEY' => $token->getPublicKey(),
                ],
            ]
        );
    }

    /**
     * @throws GuzzleException
     */
    private function requestGET(string $url, array $params = []): ResponseInterface
    {
        return $this->client->request(
            method: 'GET',
            uri: $this->binanceAPIUrl . $url . '?' . http_build_query($params),
            options: [
                'headers' => [
                    'X-MBX-APIKEY' => $this->binanceAPIKey,
                ],
            ]
        );
    }
}
