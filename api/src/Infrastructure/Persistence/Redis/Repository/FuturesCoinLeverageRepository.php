<?php

namespace App\Infrastructure\Persistence\Redis\Repository;

use App\Domain\Contract\Repository\MySQL\FuturesCoinLeverageRepositoryInterface;

class FuturesCoinLeverageRepository extends AbstractRepository implements FuturesCoinLeverageRepositoryInterface
{
    function getPrefix(): string
    {
        return "futures_coin-leverage";
    }
}
