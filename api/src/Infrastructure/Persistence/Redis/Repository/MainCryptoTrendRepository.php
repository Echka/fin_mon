<?php

namespace App\Infrastructure\Persistence\Redis\Repository;

use App\Domain\Contract\Repository\Redis\MainCryptoTrendRepositoryInterface;

class MainCryptoTrendRepository extends AbstractRepository implements MainCryptoTrendRepositoryInterface
{
    function getPrefix(): string
    {
        return "main-crypto_trend";
    }
}
