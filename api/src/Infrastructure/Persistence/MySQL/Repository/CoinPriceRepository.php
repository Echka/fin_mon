<?php

namespace App\Infrastructure\Persistence\MySQL\Repository;

use App\Domain\Contract\Repository\MySQL\CoinPriceRepositoryInterface;
use App\Domain\Entity\Coin;
use App\Domain\Entity\CoinPrice;
use DateTime;

class CoinPriceRepository extends AbstractRepository implements CoinPriceRepositoryInterface
{
    function getEntityClassName(): string
    {
        return CoinPrice::class;
    }


    public function getFromDate(DateTime $fromDate): array
    {
        return $this->createQueryBuilder('cp')
            ->where('cp.date >= DATE(:fromDate)')
            ->orderBy('cp.date', 'DESC')
            ->setParameter('fromDate', $fromDate->getTimestamp())
            ->getQuery()
            ->getResult();
    }

    public function getLastByHours(int $hours, Coin $coin): array
    {
        return $this->createQueryBuilder('cp')
            ->where('cp.coin = :coin')
            ->orderBy('cp.date', 'DESC')
            ->setMaxResults($hours)
            ->setParameter('coin', $coin->getId())
            ->getQuery()
            ->getResult();
    }

    public function getLastByDays(int $days, Coin $coin): array
    {
        return $this->createQueryBuilder('cp')
            ->where('cp.coin = :coin')
            ->andWhere("cp.date like '% 00:%'")
            ->orderBy('cp.date', 'DESC')
            ->setMaxResults($days)
            ->setParameter('coin', $coin->getId())
            ->getQuery()
            ->getResult();
    }
}
