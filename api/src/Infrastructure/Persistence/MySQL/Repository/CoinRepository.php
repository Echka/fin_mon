<?php

namespace App\Infrastructure\Persistence\MySQL\Repository;

use App\Domain\Contract\Repository\MySQL\CoinRepositoryInterface;
use App\Domain\Entity\Coin;

class CoinRepository extends AbstractRepository implements CoinRepositoryInterface
{
    function getEntityClassName(): string
    {
        return Coin::class;
    }

    public function all(): array
    {
        return $this->createQueryBuilder('c')
            ->where("c.name != 'USDT'")
            ->getQuery()
            ->getResult();
    }

    public function findByCoinName(string $coinName): Coin
    {
        return $this->createQueryBuilder('c')
            ->where("c.name like :coinName")
            ->setParameter('coinName', $coinName)
            ->getQuery()
            ->getSingleResult();
    }

    public function allInteresting(): array
    {
        return $this->createQueryBuilder('c')
            ->where("c.name != 'USDT'")
            ->andWhere("c.interest = 1")
            ->getQuery()
            ->getResult();
    }
}
