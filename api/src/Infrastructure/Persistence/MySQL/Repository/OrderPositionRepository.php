<?php

namespace App\Infrastructure\Persistence\MySQL\Repository;

use App\Domain\Contract\Repository\MySQL\OrderPositionRepositoryInterface;
use App\Domain\Entity\OrderPosition;

class OrderPositionRepository extends AbstractRepository implements OrderPositionRepositoryInterface
{
    function getEntityClassName(): string
    {
        return OrderPosition::class;
    }

    public function getAllCoinsWithOpenPosition(): array
    {
        return $this->createQueryBuilder('op')
            ->where('op.result is null')
            ->groupBy('op.coin')
            ->getQuery()
            ->getResult();
    }

    public function getAllByAlgorithmAndResult(string $algorithmName, ?int $resultStatus): array
    {
        $query = $this->createQueryBuilder('op');

        $resultStatus === OrderPosition::RESULT_NEW
            ? $query->where('op.result is null')
            : $query->where('op.result = :result')
            ->setParameter('result', $resultStatus);

        return $query
            ->andWhere("op.algorithmName like :algorithmName")
            ->setParameter(':algorithmName', $algorithmName)
            ->groupBy('op.coin')
            ->getQuery()
            ->getResult();
    }
}
