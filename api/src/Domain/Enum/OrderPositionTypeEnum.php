<?php

namespace App\Domain\Enum;

enum OrderPositionTypeEnum: int
{
    case UNCERTAINTY_TYPE = 0;
    case LONG_TYPE = 1;
    case SHORT_TYPE = 2;

    public function getTypeString(): string
    {
        return match ($this) {
            self::UNCERTAINTY_TYPE => 'UNCERTAINTY',
            self::LONG_TYPE => 'LONG',
            self::SHORT_TYPE => 'SHORT',
        };
    }
}
