<?php

namespace App\Domain\Entity;

use App\Domain\Contract\Entity\EntityInterface;
use App\Domain\Contract\Repository\MySQL\OrderPositionRepositoryInterface;
use App\Domain\Enum\OrderPositionTypeEnum;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

#[ORM\Entity(repositoryClass: OrderPositionRepositoryInterface::class)]
#[ORM\HasLifecycleCallbacks]
class OrderPosition implements EntityInterface, JsonSerializable
{
    public const RESULT_PROFIT = 1;
    public const RESULT_LOST = -1;
    public const RESULT_NEW = null;
    public const RESULT_FINISHED = 2;
    public const PERCENT_TO_PROFIT = 2;

    #[ORM\ManyToOne(targetEntity: Coin::class, inversedBy: 'transactions')]
    #[ORM\JoinColumn(name: 'coin_id', referencedColumnName: 'id', nullable: false)]
    private $coin;
    #[ORM\Column(type: 'datetime')]
    private $date;
    #[ORM\Column(type: 'datetime', nullable: true, name: 'close_date')]
    private $closeDate;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;
    #[ORM\Column(type: 'integer', name: 'leverage')]
    private $leverage;
    #[ORM\Column(type: 'decimal', name: 'market_price', precision: 16, scale: 8)]
    private $marketPrice;
    #[ORM\Column(type: 'decimal', name: 'close_price', precision: 16, scale: 8, nullable: true)]
    private $closePrice;
    #[ORM\Column(type: 'decimal', name: 'close_percent', precision: 16, scale: 8, nullable: true)]
    private $closePercent;
    #[ORM\Column(type: 'integer', name: 'profit_percent')]
    private $profitPercent;
    #[ORM\Column(type: 'integer', nullable: true)]
    private $result;
    #[ORM\Column(type: 'integer', enumType: OrderPositionTypeEnum::class)]
    private OrderPositionTypeEnum $type;
    #[ORM\Column(type: 'string', name: 'algorithm_name', nullable: true)]
    private $algorithmName;

    public function setType(OrderPositionTypeEnum $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'leverage' => $this->getLeverage(),
            'type' => $this->getType()->getTypeString(),
            'market_price' => $this->getMarketPrice(),
            'result' => $this->getResult(),
            'close_percent' => $this->getClosePercent(),
            'close_price' => $this->getClosePrice(),
            'close_date' => $this->getCloseDate(),
            'profit_percent' => $this->getProfitPercent(),
            'date' => $this->getDate(),
            'coin' => $this->getCoin(),
            'algorithm_name' => $this->getAlgorithmName(),
        ];
    }

    public function getMarketPrice(): float
    {
        return $this->marketPrice;
    }

    public function setMarketPrice(float $marketPrice): self
    {
        $this->marketPrice = $marketPrice;

        return $this;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCoin(): Coin
    {
        return $this->coin;
    }

    public function setCoin(Coin $coin): self
    {
        $this->coin = $coin;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getProfitPrice(): float
    {
        $profitPrice = $this->getMarketPrice();
        $profitPrice += ($this->getType() === OrderPositionTypeEnum::LONG_TYPE ? 1 : -1) * $this->getMarketPrice(
            ) * ($this->getProfitPercent() / 100);

        return $profitPrice;
    }

    public function getProfitPercent(): int
    {
        return $this->profitPercent;
    }

    public function setProfitPercent(int $profitPercent): self
    {
        $this->profitPercent = $profitPercent;

        return $this;
    }

    public function getLiquidationPrice(): float
    {
        return $this->getMarketPrice() - $this->getLiquidationPercent() * $this->getMarketPrice();
    }

    public function getLiquidationPercent(): float
    {
        return 100 / $this->getLeverage();
    }

    public function getLeverage(): int
    {
        return $this->leverage;
    }

    public function setLeverage(int $leverage): self
    {
        $this->leverage = $leverage;

        return $this;
    }

    public function setResult(bool $result): self
    {
        $this->result = $result ? self::RESULT_PROFIT : self::RESULT_LOST;

        return $this;
    }

    public function getResult(): ?int
    {
        return $this->result ?? self::RESULT_NEW;
    }

    public function getClosePercent(): ?float
    {
        return $this->closePercent;
    }

    public function getClosePrice(): ?float
    {
        return $this->closePrice;
    }

    public function getType(): OrderPositionTypeEnum
    {
        return $this->type;
    }

    public function getCloseDate(): ?DateTime
    {
        return $this->closeDate;
    }

    public function getAlgorithmName(): ?string
    {
        return $this->algorithmName;
    }

    public function setAlgorithmName(string $algorithmName): self
    {
        $this->algorithmName = $algorithmName;

        return $this;
    }

    public function closePosition(float $closePrice): self
    {
        $priceDelta = $closePrice > $this->getMarketPrice();
        $result = ($this->getType() === OrderPositionTypeEnum::LONG_TYPE && $priceDelta)
            || ($this->getType() === OrderPositionTypeEnum::SHORT_TYPE && $priceDelta === false);

        $this->setResult($result);
        $this->closePercent = (($closePrice / $this->getMarketPrice()) * 100) - 100;
        $this->closeDate = new DateTime();
        $this->closePrice = $closePrice;

        $this->calculateProfitPercent();

        return $this;
    }

    public function setCloseDate($closeDate): self
    {
        $this->closeDate = $closeDate;

        return $this;
    }

    public function calculateProfitPercent(): self
    {
        $profitPercent = $this->getType() === OrderPositionTypeEnum::SHORT_TYPE
            ? $this->closePercent * -1
            : $this->closePercent;

        return $this->setProfitPercent((int)$profitPercent);
    }
}
