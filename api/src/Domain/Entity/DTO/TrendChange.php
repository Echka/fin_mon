<?php

namespace App\Domain\Entity\DTO;

use App\Domain\Enum\OrderPositionTypeEnum;

class TrendChange
{
    private const SCALE = 12;
    private string $ma = '0';
    private string $negativeDelta = '0';
    private string $positiveDelta = '0';
    private array $previous = [];
    private string $price = '0';
    private string $priceDelta = '0';
    private string $rs = '0';
    private string $rsi = '0';

    public function __construct(
        private readonly int $countMA,
    ) {
    }

    public function addPrice(string $price): self
    {
        $this->price = $price;

        $this->setPriceDelta();
        $this->setDeltas();
        $this->setRS();
        $this->setRSI();
        $this->setMA();

        $this->previous[] = [
            'price' => $this->price,
            'priceDelta' => $this->priceDelta,
            'positiveDelta' => $this->positiveDelta,
            'negativeDelta' => $this->negativeDelta,
            'rs' => $this->rs,
            'rsi' => $this->rsi,
            'ma' => $this->ma,
        ];

        return $this;
    }

    private function setPriceDelta(): self
    {
        $this->priceDelta = count($this->previous) > 0
            ? bcsub($this->price, end($this->previous)['price'], self::SCALE)
            : 0;

        return $this;
    }

    private function setDeltas(): self
    {
        if (count($this->previous) < $this->countMA) {
            return $this;
        }

        $positiveDelta = $negativeDelta = '0';

        for ($i = count($this->previous) - 1, $k = 0; $k < $this->countMA; $k++, $i--) {
            $comp = bccomp($this->previous[$i]['priceDelta'], 0, self::SCALE);

            if ($comp === 1) {
                $positiveDelta = bcadd($positiveDelta, $this->previous[$i]['priceDelta'], self::SCALE);
            } else {
                if ($comp === -1) {
                    $negativeDelta = bcadd($negativeDelta, $this->previous[$i]['priceDelta'], self::SCALE);
                }
            }
        }

        $this->positiveDelta = bcdiv(str_replace('-', '', $positiveDelta), $this->countMA, self::SCALE);
        $this->negativeDelta = bcdiv(str_replace('-', '', $negativeDelta), $this->countMA, self::SCALE);

        return $this;
    }

    private function setRS(): self
    {
        $this->rs = bccomp($this->negativeDelta, '0', self::SCALE) === 1
            ? bcdiv($this->positiveDelta, $this->negativeDelta, self::SCALE)
            : 0;

        return $this;
    }

    private function setRSI(): self
    {
        $rsi = bcadd('1', $this->rs, self::SCALE);
        $rsi = bcdiv('100', $rsi, self::SCALE);
        $this->rsi = bcsub('100', $rsi, self::SCALE);

        return $this;
    }

    private function setMA(): self
    {
        if (count($this->previous) < $this->countMA * 2) {
            return $this;
        }

        $ma = '0';

        for ($i = count($this->previous) - 1, $k = 0; $k < $this->countMA; $k++, $i--) {
            $ma = bcadd($ma, $this->previous[$i]['rsi'], self::SCALE);
        }

        $this->ma = bcdiv($ma, $this->countMA, self::SCALE);

        return $this;
    }

    public function getTrend(): OrderPositionTypeEnum
    {
        if (count($this->previous) < $this->countMA * 2) {
            return OrderPositionTypeEnum::UNCERTAINTY_TYPE;
        }

        $countPrevious = count($this->previous);

        $checkRSI = bccomp($this->previous[$countPrevious - 1]['rsi'], $this->previous[$countPrevious - 2]['rsi']);
        $checkMA = bccomp($this->previous[$countPrevious - 1]['rsi'], $this->previous[$countPrevious - 1]['ma']);

        $checkShort = $checkRSI === -1 && $checkMA === -1;
        $checkLong = $checkRSI === 1 && $checkMA === 1;

        return $checkShort
            ? OrderPositionTypeEnum::SHORT_TYPE
            : ($checkLong ? OrderPositionTypeEnum::LONG_TYPE : OrderPositionTypeEnum::UNCERTAINTY_TYPE);
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public function getLastRSI(): string
    {
        return $this->previous[count($this->previous) - 2]['rsi'];
    }

    public function getRSI(): string
    {
        return $this->rsi;
    }

    public function getMA(): string
    {
        return $this->ma;
    }
}
