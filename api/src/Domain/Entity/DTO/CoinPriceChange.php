<?php

namespace App\Domain\Entity\DTO;

use App\Domain\Entity\CoinPrice;
use App\Domain\Entity\OrderPosition;
use App\Domain\Entity\Coin;
use App\Domain\Enum\OrderPositionTypeEnum;

class CoinPriceChange
{
    private ?float $changePercent = null;
    private CoinPrice $finishPrice;
    private int $needPercent;
    private CoinPrice $startPrice;
    private Coin $coin;

    public function setStartPrice(CoinPrice $coinPrice): self
    {
        $this->startPrice = $coinPrice;

        return $this;
    }

    public function setFinishPrice(CoinPrice $coinPrice): self
    {
        $this->finishPrice = $coinPrice;

        return $this;
    }

    public function setNeedPercent(int $needPercent): self
    {
        $this->needPercent = $needPercent;

        return $this;
    }

    public function getChangePercent(): float
    {
        return $this->changePercent;
    }

    public function calcChangePercent(): self
    {
        $this->changePercent = $this->startPrice->getMarketPrice()
            ? (($this->startPrice->getMarketPrice() / $this->finishPrice->getMarketPrice()) * 100) - 100
            : 0;

        return $this;
    }

    public function getMarketPrice(): float
    {
        return $this->startPrice->getMarketPrice();
    }

    public function getNeedOrder(): bool
    {
        return abs($this->changePercent) >= $this->needPercent;
    }

    public function getOrderType(): OrderPositionTypeEnum
    {
        return $this->changePercent >= 0
            ? OrderPositionTypeEnum::LONG_TYPE
            : OrderPositionTypeEnum::SHORT_TYPE;
    }

    public function setCoin(Coin $coin): self
    {
        $this->coin = $coin;

        return $this;
    }

    public function getCoin(): Coin
    {
        return $this->coin;
    }

    public function getPreviousMarketPrice(): float
    {
        return $this->finishPrice->getMarketPrice();
    }
}
