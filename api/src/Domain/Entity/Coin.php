<?php

namespace App\Domain\Entity;

use App\Domain\Contract\Entity\EntityInterface;
use App\Domain\Contract\Repository\MySQL\CoinRepositoryInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CoinRepositoryInterface::class)]
#[ORM\HasLifecycleCallbacks]
class Coin implements EntityInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;
    #[ORM\Column(type: 'string', unique: true)]
    private $name;
    #[ORM\Column(name: 'full_name', type: 'string', unique: true, nullable: true)]
    private $fullName;
    #[ORM\Column(name: 'interest', type: 'boolean', nullable: true)]
    private $interest = false;
    #[ORM\OneToMany(mappedBy: 'coin', targetEntity: CoinPrice::class)]
    private $operations;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPairName(): string
    {
        return $this->name . 'USDT';
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(?string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function isInterest(): bool
    {
        return $this->interest;
    }

    public function setInterest(bool $interest): self
    {
        $this->interest = $interest;

        return $this;
    }
}
