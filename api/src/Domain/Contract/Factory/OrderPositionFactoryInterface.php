<?php

namespace App\Domain\Contract\Factory;

use App\Domain\Entity\Coin;
use App\Domain\Entity\DTO\CoinPriceChange;
use App\Domain\Entity\DTO\TrendChange;
use App\Domain\Entity\OrderPosition;

interface OrderPositionFactoryInterface
{
    public function getInstance(): OrderPosition;

    public function getForCoinPriceChangeAlgorithm(CoinPriceChange $coinPriceChange): OrderPosition;

    public function getForTrendChangeAlgorithm(TrendChange $trendChange, Coin $coin): OrderPosition;
}
