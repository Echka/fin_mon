<?php

namespace App\Domain\Contract\Service\Notification;

interface NotificationServiceInterface
{
    public function notification(string $algorithm, array $userList, array $data): void;
}
