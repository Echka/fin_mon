<?php

namespace App\Domain\Contract\Algorithm;

interface AlgorithmInterface
{
    const COIN_PRICE_SPOT_ALGORITHM = 'coin_price_spot';
    const TREND_CHANGE_ALGORITHM = 'trend_change';
    const MAIN_CRYPTO_TREND_ALGORITHM = 'main_crypto_trend';
    const ALGORITHM_LIST = [
        self::COIN_PRICE_SPOT_ALGORITHM,
        self::TREND_CHANGE_ALGORITHM,
        self::MAIN_CRYPTO_TREND_ALGORITHM,
    ];
    const ALGORITHM_SETTING_NOTIFICATION = 'notification';
    const ALGORITHM_SETTING_REAL_BUY = 'real_buy';
    const ALGORITHM_SETTING_FIELDS = [
        self::ALGORITHM_SETTING_NOTIFICATION,
        self::ALGORITHM_SETTING_REAL_BUY,
    ];
    const ALGORITHM_DEFAULT_SETTING = [
        self::ALGORITHM_SETTING_NOTIFICATION => false,
        self::ALGORITHM_SETTING_REAL_BUY => false,
    ];

    public function getName(): string;
    public function handle(): void;
}
