<?php

namespace App\Domain\Contract\Repository\MySQL;

use DateTime;

interface OperationRepositoryInterface
{
    public function getStatisticByUser(int $userId): array;

    public function getExternalIdsFromTime(DateTime $dateTime): array;

    public function getByFilters(array $filters): array;
}
