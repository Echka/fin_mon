<?php

namespace App\Domain\Contract\Repository\MySQL;

use App\Domain\Entity\Coin;
use DateTime;

interface CoinPriceRepositoryInterface
{
    public function getFromDate(DateTime $fromDate): array;

    public function getLastByHours(int $hours, Coin $coin): array;

    public function getLastByDays(int $days, Coin $coin): array;
}
