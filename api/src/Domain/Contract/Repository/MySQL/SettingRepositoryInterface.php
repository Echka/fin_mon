<?php

namespace App\Domain\Contract\Repository\MySQL;

interface SettingRepositoryInterface
{
    public function getMonobankNotNullList(): array;

    public function getBinanceKeysNotNullList(): array;
}
