<?php

namespace App\Domain\Contract\Repository\MySQL;

use App\Domain\Entity\Coin;

interface CoinRepositoryInterface
{
    public function allInteresting(): array;

    public function findByCoinName(string $coinName): Coin;
}
