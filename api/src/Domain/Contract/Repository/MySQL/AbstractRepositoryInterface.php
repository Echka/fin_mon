<?php

namespace App\Domain\Contract\Repository\MySQL;

use App\Domain\Contract\Entity\EntityInterface;

interface AbstractRepositoryInterface
{
    function store(EntityInterface $entity): void;

    function all(): array;

    function startTransaction(): void;

    function commitTransaction(): void;

    function update(EntityInterface $entity): void;

    function delete(EntityInterface $entity): void;

    function findByOne(int $id): ?EntityInterface;

    function getEntityClassName(): string;

    function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null);
}
