<?php

namespace App\Domain\Contract\Repository\MySQL;

use App\Domain\Entity\User;

interface UserRepositoryInterface
{
    public function getByUsername(string $username): ?User;

    public function getByTelegramChatId(int $chatId): ?User;

    public function getTelegramChatIdNotNullList(): array;
}
