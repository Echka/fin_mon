<?php

namespace App\Domain\Contract\Repository\MySQL;

interface OrderPositionRepositoryInterface
{
    public function getAllCoinsWithOpenPosition(): array;

    public function getAllByAlgorithmAndResult(string $algorithmName, ?int $resultStatus): array;
}
