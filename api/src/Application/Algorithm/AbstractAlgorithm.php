<?php

namespace App\Application\Algorithm;

use App\Domain\Contract\Algorithm\AlgorithmInterface;
use App\Domain\Contract\Repository\MySQL\OrderPositionRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\UserRepositoryInterface;
use App\Domain\Contract\Service\Notification\NotificationServiceInterface;
use App\Domain\Entity\OrderPosition;
use App\Domain\Entity\User;

abstract class AbstractAlgorithm implements AlgorithmInterface
{
    protected array $openOrderPositions = [];

    public function __construct(
        private readonly OrderPositionRepositoryInterface $orderPositionRepository,
        private readonly UserRepositoryInterface $userRepository,
        private readonly NotificationServiceInterface $notificationService,
    ) {
    }

    protected function setOpenOrderPositions(?int $resultStatus = OrderPosition::RESULT_NEW): void
    {
        $openOrderPositions = $this->orderPositionRepository->getAllByAlgorithmAndResult(
            $this->getName(),
            $resultStatus
        );
        $openOrderPositionsByCoinIds = [];

        /* @var OrderPosition $openOrderPosition */
        foreach ($openOrderPositions as $openOrderPosition) {
            $openOrderPositionsByCoinIds[$openOrderPosition->getCoin()->getId()] = $openOrderPosition;
        }

        $this->openOrderPositions = $openOrderPositionsByCoinIds;
    }

    protected function notification(array $data): void
    {
        $users = $this->userRepository->getTelegramChatIdNotNullList();

        $users = array_filter(
            $users,
            function (User $user) {
                return $user->getSetting()->getNotificationAccessByAlgorithmName($this->getName());
            }
        );

        $this->notificationService->notification(
            $this->getName(),
            $users,
            $data,
        );
    }
}
