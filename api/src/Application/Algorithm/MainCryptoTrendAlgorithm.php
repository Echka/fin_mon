<?php

namespace App\Application\Algorithm;

use App\Domain\Contract\Repository\MySQL\CoinPriceRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\CoinRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\OrderPositionRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\UserRepositoryInterface;
use App\Domain\Contract\Repository\Redis\MainCryptoTrendRepositoryInterface;
use App\Domain\Contract\Repository\Redis\SpotCoinPriceRepositoryInterface;
use App\Domain\Contract\Service\Notification\NotificationServiceInterface;
use App\Domain\Entity\Coin;
use App\Domain\Entity\CoinPrice;
use App\Domain\Entity\DTO\TrendChange;

class MainCryptoTrendAlgorithm extends AbstractAlgorithm
{
    private const COUNT_DAY_TO_CHECK_MA = 30;

    public function __construct(
        private readonly CoinPriceRepositoryInterface $coinPriceRepository,
        OrderPositionRepositoryInterface $orderPositionRepository,
        private readonly CoinRepositoryInterface $coinRepository,
        private readonly SpotCoinPriceRepositoryInterface $spotCoinPriceRepository,
        UserRepositoryInterface $userRepository,
        NotificationServiceInterface $notificationService,
        private readonly MainCryptoTrendRepositoryInterface $mainCryptoTrendRepository,
    ) {
        parent::__construct(
            $orderPositionRepository,
            $userRepository,
            $notificationService,
        );
    }

    public function handle(): void
    {
        $this->setOpenOrderPositions();

        $coin = $this->coinRepository->findByCoinName('BTC');

        $trendChange = $this->checkTrendByCoin($coin);

        $this->checkOnTrendChange(
            $trendChange,
            $coin,
        );
    }

    private function checkTrendByCoin(Coin $coin): TrendChange
    {
        $trendChange = new TrendChange(self::COUNT_DAY_TO_CHECK_MA);

        $prices = $this->coinPriceRepository->getLastByDays(
            days: self::COUNT_DAY_TO_CHECK_MA * 2 + 2,
            coin: $coin,
        );

        /* @var CoinPrice $price */
        foreach ($prices as $price) {
            $trendChange->addPrice($price->getMarketPrice());
        }

        return $trendChange->addPrice(
            $this->spotCoinPriceRepository->find($coin->getPairName())
        );
    }

    private function checkOnTrendChange(TrendChange $trendChange, Coin $coin): void
    {
        $this->mainCryptoTrendRepository->save(
            $coin->getName(),
            $trendChange->getTrend()->value,
        );
    }

    public function getName(): string
    {
        return self::MAIN_CRYPTO_TREND_ALGORITHM;
    }
}
