<?php

namespace App\Application\Algorithm;

use App\Application\Factory\DTO\CoinPriceChangeFactory;
use App\Domain\Contract\Factory\OrderPositionFactoryInterface;
use App\Domain\Contract\Repository\MySQL\CoinPriceRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\OrderPositionRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\UserRepositoryInterface;
use App\Domain\Contract\Service\Notification\NotificationServiceInterface;
use App\Domain\Entity\CoinPrice;
use App\Domain\Entity\OrderPosition;
use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Throwable;

class CoinPriceSpotAlgorithm extends AbstractAlgorithm
{
    public const RANGE_MIN = 1;
    public const RANGE_TWO_MIN = 2;
    public const RANGE_FIVE_MIN = 5;
    public const RANGE_TEN_MIN = 10;
    public const RANGE_FIFTEEN_MIN = 15;
    public const MAX_RANGE_MIN = self::RANGE_FIFTEEN_MIN;
    public const RANGE_TO_PERCENT = [
        self::RANGE_MIN => 2,
        self::RANGE_TWO_MIN => 3,
        self::RANGE_FIVE_MIN => 5,
        self::RANGE_TEN_MIN => 10,
        self::RANGE_FIFTEEN_MIN => 15,
    ];

    public function __construct(
        private readonly CoinPriceChangeFactory $coinPriceChangeFactory,
        private readonly CoinPriceRepositoryInterface $coinPriceRepository,
        private readonly OrderPositionFactoryInterface $orderPositionFactory,
        private readonly OrderPositionRepositoryInterface $orderPositionRepository,
        UserRepositoryInterface $userRepository,
        NotificationServiceInterface $notificationService,
    ) {
        parent::__construct(
            $orderPositionRepository,
            $userRepository,
            $notificationService,
        );
    }

    public function getName(): string
    {
        return self::COIN_PRICE_SPOT_ALGORITHM;
    }

    public function handle(): void
    {
        $this->setOpenOrderPositions();

        $maxMinuteInterval = new DateInterval("PT" . (self::MAX_RANGE_MIN + 1) . "M");
        $dateTimeFrom = (new DateTime())->sub($maxMinuteInterval);
        $coinPricesByCoins = $this->getCoinPricesByCoins($dateTimeFrom);

        $coinPricesByCoins->map(function ($coinPrices) {
            try {
                $this->checkOnCoinPriceChange($coinPrices);
            } catch (Throwable) {
            }
        });
    }

    private function getCoinPricesByCoins(DateTime $fromDate): ArrayCollection
    {
        $coinPrices = $this->coinPriceRepository->getFromDate($fromDate);
        $coinPricesByCoins = new ArrayCollection();

        /* @var CoinPrice $coinPrice */
        foreach ($coinPrices as $coinPrice) {
            $coinId = $coinPrice->getCoin()->getId();

            if ($coinPricesByCoins->get($coinId) === null) {
                $coinPricesByCoins->set(
                    $coinId,
                    new ArrayCollection(),
                );
            }

            $coinPricesByCoins->get($coinId)
                ->add($coinPrice);
        }

        return $coinPricesByCoins;
    }

    private function checkOnCoinPriceChange(ArrayCollection $coinPrices): void
    {
        foreach (self::RANGE_TO_PERCENT as $min => $needPercent) {
            /* @var CoinPrice $startPrice */
            $startPrice = $coinPrices->first();
            /* @var CoinPrice|null $startPrice */
            $finishPrice = $coinPrices->get($min);

            $coinPriceChange = $this->coinPriceChangeFactory->getCoinPriceChange(
                $startPrice,
                $finishPrice,
                $needPercent,
            );

            if ($coinPriceChange->getNeedOrder() === false) {
                return;
            }

            $this->checkAndCreateDemoOrder(
                $this->orderPositionFactory->getForCoinPriceChangeAlgorithm($coinPriceChange)
            );

            $this->notification([
                'coinName' => $coinPriceChange->getCoin()->getName(),
                'min' => $min,
                'changePercent' => $coinPriceChange->getChangePercent(),
                'orderType' => $coinPriceChange->getOrderType(),
                'previousMarketPrice' => $coinPriceChange->getPreviousMarketPrice(),
                'marketPrice' => $coinPriceChange->getMarketPrice(),
            ]);
        }
    }

    private function checkAndCreateDemoOrder(OrderPosition $orderPosition): void
    {
        if (isset($this->openOrderPositions[$orderPosition->getCoin()->getId()]) === false) {
            $this->openOrderPositions[$orderPosition->getCoin()->getId()] = true;

            $this->orderPositionRepository->store($orderPosition);
        }
    }
}
