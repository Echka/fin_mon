<?php

namespace App\Application\Algorithm;

use App\Domain\Contract\Factory\OrderPositionFactoryInterface;
use App\Domain\Contract\Repository\MySQL\CoinPriceRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\CoinRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\OrderPositionRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\UserRepositoryInterface;
use App\Domain\Contract\Repository\Redis\MainCryptoTrendRepositoryInterface;
use App\Domain\Contract\Repository\Redis\SpotCoinPriceRepositoryInterface;
use App\Domain\Contract\Service\Notification\NotificationServiceInterface;
use App\Domain\Entity\Coin;
use App\Domain\Entity\CoinPrice;
use App\Domain\Entity\DTO\TrendChange;
use App\Domain\Entity\OrderPosition;
use App\Domain\Enum\OrderPositionTypeEnum;
use Throwable;

class TrendChangeAlgorithm extends AbstractAlgorithm
{
    private const COUNT_HOUR_TO_CHECK_MA = 30;

    public function __construct(
        private readonly CoinPriceRepositoryInterface $coinPriceRepository,
        private readonly OrderPositionRepositoryInterface $orderPositionRepository,
        private readonly CoinRepositoryInterface $coinRepository,
        private readonly OrderPositionFactoryInterface $orderPositionFactory,
        private readonly SpotCoinPriceRepositoryInterface $spotCoinPriceRepository,
        UserRepositoryInterface $userRepository,
        NotificationServiceInterface $notificationService,
        private readonly MainCryptoTrendRepositoryInterface $mainCryptoTrendRepository,
    ) {
        parent::__construct(
            $orderPositionRepository,
            $userRepository,
            $notificationService,
        );
    }

    public function getName(): string
    {
        return self::TREND_CHANGE_ALGORITHM;
    }

    public function handle(): void
    {
        $this->setOpenOrderPositions();

        $coins = $this->coinRepository->allInteresting();

        /** @var Coin $coin */
        foreach ($coins as $coin) {
            try {
                $trendChange = $this->checkTrendByCoin($coin);

                $this->checkOnTrendChange(
                    $trendChange,
                    $coin,
                );
            } catch (Throwable $exception) {
            }
        }
    }

    private function checkTrendByCoin(Coin $coin): TrendChange
    {
        $trendChange = new TrendChange(self::COUNT_HOUR_TO_CHECK_MA);

        $prices = $this->coinPriceRepository->getLastByHours(
            hours: self::COUNT_HOUR_TO_CHECK_MA * 2 + 2,
            coin: $coin,
        );

        /* @var CoinPrice $price */
        foreach ($prices as $price) {
            $trendChange->addPrice($price->getMarketPrice());
        }

        return $trendChange->addPrice(
            $this->spotCoinPriceRepository->find($coin->getPairName())
        );
    }

    private function checkOnTrendChange(TrendChange $trendChange, Coin $coin): void
    {
        $currentTrend = $trendChange->getTrend();

        /* @var null|OrderPosition $openOrderPosition */
        $openOrderPosition = $this->openOrderPositions[$coin->getId()] ?? null;
        $isOpenPositionWithCurrentTrend = $openOrderPosition
            && $currentTrend == $this->openOrderPositions[$coin->getId()]->getType();

        if (
            $currentTrend === OrderPositionTypeEnum::UNCERTAINTY_TYPE
            || $isOpenPositionWithCurrentTrend
        ) {
            return;
        }

        if ($openOrderPosition) {
            $this->orderPositionRepository->update(
                $openOrderPosition->closePosition($trendChange->getPrice())
            );
        }

        /*$mainCryptoTrend = $this->mainCryptoTrendRepository->find('BTC');

        if (
            $mainCryptoTrend
            && $mainCryptoTrend != $trendChange->getTrend()->value
        ) {
             TODO add after 2 months
            return;
        }*/

        $this->orderPositionRepository->store(
            $this->orderPositionFactory->getForTrendChangeAlgorithm($trendChange, $coin)
        );

        $this->notification([
            'coinName' => $coin->getName(),
            'orderType' => $currentTrend->getTypeString(),
            'lastRSI' => $trendChange->getLastRSI(),
            'RSI' => $trendChange->getRSI(),
            'MA' => $trendChange->getMA(),
        ]);
    }
}
