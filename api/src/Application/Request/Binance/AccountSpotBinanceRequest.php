<?php

namespace App\Application\Request\Binance;

use GuzzleHttp\Exception\GuzzleException;

class AccountSpotBinanceRequest extends AbstractBinanceRequest
{
    /**
     * @throws GuzzleException
     */
    public function sendRequest(): array
    {
        $response = $this->binanceClient->getSpotAccountData(
            $this->getRequestToken(),
        );

        return json_decode($response->getBody()->getContents(), true);
    }
}
