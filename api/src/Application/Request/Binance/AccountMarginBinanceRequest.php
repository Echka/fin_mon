<?php

namespace App\Application\Request\Binance;

use GuzzleHttp\Exception\GuzzleException;

class AccountMarginBinanceRequest extends AbstractBinanceRequest
{
    /**
     * @throws GuzzleException
     */
    public function sendRequest(): array
    {
        $response = $this->binanceClient->getMarginAccountData(
            $this->getRequestToken(),
        );

        return json_decode($response->getBody()->getContents(), true);
    }
}
