<?php

namespace App\Application\Request\Binance;

use GuzzleHttp\Exception\GuzzleException;

class CoinPriceRequest extends AbstractBinanceRequest
{
    /**
     * @throws GuzzleException
     */
    public function sendRequest(array $coinNames): array
    {
        $response = $this->binanceClient->getCoinPrice($coinNames);

        return json_decode($response->getBody()->getContents(), true);
    }
}
