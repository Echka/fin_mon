<?php

namespace App\Application\Service\Notification;

use App\Application\Service\TelegramService;
use App\Domain\Contract\Service\Notification\NotificationServiceInterface;
use App\Domain\Entity\User;
use DateTime;

class TrendChangeNotificationService implements NotificationServiceInterface
{
    private const NOTIFICATION_TEXT =
        "Coin: %s\r\n" .
        "Type: %s\r\n" .
        "Last RSI: %d\r\n" .
        "RSI: %d\r\n" .
        "MA: %d\r\n" .
        "Time: %s\r\n" .
        "Algorithm: %s";

    public function __construct(
        private readonly TelegramService $telegramService,
    ) {
    }

    public function notification(
        string $algorithm,
        array $userList,
        array $data,
    ): void {
        $message = sprintf(
            self::NOTIFICATION_TEXT,
            $data['coinName'],
            $data['orderType'],
            $data['lastRSI'],
            $data['RSI'],
            $data['MA'],
            (new DateTime())->format('Y-m-d H:i:s'),
            $algorithm,
        );

        /* @var User $user */
        foreach ($userList as $user) {
            $this->telegramService->sendMessage(
                $user->getTelegramChatId(),
                $message
            );
        }
    }
}
