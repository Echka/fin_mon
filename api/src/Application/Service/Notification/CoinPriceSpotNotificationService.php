<?php

namespace App\Application\Service\Notification;

use App\Application\Service\TelegramService;
use App\Domain\Contract\Service\Notification\NotificationServiceInterface;
use App\Domain\Entity\User;
use DateTime;

class CoinPriceSpotNotificationService implements NotificationServiceInterface
{
    private const NOTIFICATION_TEXT =
        "Coin: %s\r\n" .
        "TimeRange: %d min\r\n" .
        "ChangePercent: %.2F\r\n" .
        "Position: %s\r\n" .
        "PreviousPrice: %.8F\r\n" .
        "CurrentPrice: %.8F\r\n" .
        "Time: %s\r\n" .
        "Algorithm: %s";

    public function __construct(
        private readonly TelegramService $telegramService,
    ) {
    }

    public function notification(
        string $algorithm,
        array $userList,
        array $data,
    ): void {
        $message = sprintf(
            self::NOTIFICATION_TEXT,
            $data['coinName'],
            $data['min'],
            $data['changePercent'],
            $data['orderType'] ? 'LONG' : 'SHORT',
            $data['previousMarketPrice'],
            $data['marketPrice'],
            (new DateTime())->format('Y-m-d H:i:s'),
            $algorithm,
        );

        /* @var User $user */
        foreach ($userList as $user) {
            $this->telegramService->sendMessage(
                $user->getTelegramChatId(),
                $message
            );
        }
    }
}
