<?php

namespace App\Application\Service;

use App\Application\Request\Binance\CoinListRequest;
use App\Domain\Contract\Factory\CoinFactoryInterface;
use App\Domain\Contract\Repository\MySQL\CoinRepositoryInterface;
use App\Domain\Entity\Coin;
use App\Domain\Exception\EntityExistException;
use Doctrine\Common\Collections\ArrayCollection;

class CoinService
{
    private ?ArrayCollection $coinList = null;

    public function __construct(
        private CoinRepositoryInterface $coinRepository,
        private CoinListRequest $coinListRequest,
        private CoinFactoryInterface $coinFactory,
    ) {
    }

    public function fillCoinsFromBinance(): void
    {
        $coinList = $this->coinListRequest->sendRequest();

        foreach ($coinList as $coin) {
            try {
                $coinEntity = $this->coinFactory->fillEntityFromBinance(
                    entity: $this->coinFactory->getInstance(),
                    data: $coin,
                );

                $this->checkOnExist($coinEntity);

                $this->coinRepository->store($coinEntity);
            } catch (EntityExistException) {
            }
        }
    }

    public function getCoinByName(string $name): ?Coin
    {
        if ($this->coinList === null) {
            $this->setCoinList();
        }

        return $this->coinList->get($name);
    }

    /**
     * @throws EntityExistException
     * */
    private function checkOnExist(Coin $coin): void
    {
        $coin = $this->getCoinByName($coin->getName());

        if ($coin) {
            throw new EntityExistException();
        }
    }

    private function setCoinList(): void
    {
        $this->coinList = new ArrayCollection();

        /* @var Coin $coin */
        foreach ($this->coinRepository->all() as $coin) {
            $this->coinList->set(
                $coin->getName(),
                $coin,
            );
        }
    }
}
