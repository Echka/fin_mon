<?php

namespace App\Application\Command\Operation\List;

use App\Application\Command\AbstractCommand;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @OA\Schema(
 *     example={
 *          "filters": {
 *              "from": "2000-01-01",
 *              "to": "2000-01-01",
 *              "archive": false,
 *          }
 *     }
 * )
 */
class OperationListCommand extends AbstractCommand
{
    /**
     * @Assert\Collection(
     *      fields={
     *          "from" = @Assert\Date(),
     *          "to" = @Assert\Date(),
     *          "archive" = @Assert\Type("string")
     *      },
     *      allowMissingFields=true
     * )
     * @OA\Property(
     *     type="array",
     *     @OA\Items()
     * )
     */
    protected ?array $filters = null;

    public function getFilters(): ?array
    {
        if (isset($this->filters['archive'])) {
            $this->filters['archive'] = (bool)$this->filters['archive'];
        }

        return $this->filters;
    }
}
