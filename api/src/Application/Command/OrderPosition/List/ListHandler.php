<?php

namespace App\Application\Command\OrderPosition\List;

use App\Domain\Contract\Repository\MySQL\OrderPositionRepositoryInterface;
use GuzzleHttp\Psr7\Response as HttpResponse;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ListHandler implements MessageHandlerInterface
{
    public function __construct(
        private OrderPositionRepositoryInterface $orderPositionRepository,
        private SerializerInterface $serializer,
    ) {
    }

    public function __invoke(ListCommand $command): ResponseInterface
    {
        return new HttpResponse(
            status: Response::HTTP_OK,
            body: $this->serializer->serialize(
                data: $this->orderPositionRepository->all(),
                format: 'json'
            )
        );
    }
}
