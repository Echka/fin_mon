<?php

namespace App\Application\Command\Telegram\SendMessage;

use App\Infrastructure\Adapter\TelegramAdapter;
use Exception;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SendMessageHandler implements MessageHandlerInterface
{
    public function __construct(
        private TelegramAdapter $telegramAdapter,
    ) {
    }

    /**
     * @throws Exception
     */
    public function __invoke(SendMessageCommand $command): void
    {
        $this->telegramAdapter->sendMessage(
            $command->getTelegramChatId(),
            $command->getMessage(),
        );
    }
}
