<?php

namespace App\Application\Command\CheckByAlgorithm;

use App\Domain\Contract\Algorithm\AlgorithmInterface;
use Exception;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CheckByAlgorithmHandler implements MessageHandlerInterface
{
    private ?AlgorithmInterface $algorithm = null;

    public function __construct(
        private array $algorithms,
    ) {
    }

    /**
     * @throws Exception
     */
    public function __invoke(CheckByAlgorithmCommand $command): void
    {
        $this->setAlgorithm($command);

        $this->algorithm->handle();
    }

    /**
     * @throws Exception
     */
    private function setAlgorithm(CheckByAlgorithmCommand $command): void
    {
        /* @var AlgorithmInterface $algorithm */
        foreach ($this->algorithms as $algorithm) {
            if ($algorithm->getName() === $command->getAlgorithmName()) {
                $this->algorithm = $algorithm;
                break;
            }
        }

        if ($this->algorithm === null) {
            throw new Exception('Algorithm not found');
        }
    }
}
