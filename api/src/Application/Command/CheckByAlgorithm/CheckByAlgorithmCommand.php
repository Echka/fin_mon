<?php

namespace App\Application\Command\CheckByAlgorithm;

use App\Application\Command\AbstractCommand;
use App\Domain\Contract\Algorithm\AlgorithmInterface;
use App\Domain\Contract\Command\SyncCommandInterface;
use Exception;

class CheckByAlgorithmCommand extends AbstractCommand implements SyncCommandInterface
{
    private string $algorithmName;

    /**
     * @throws Exception
     */
    public function setAlgorithmName(string $algorithmName): self
    {
        if (in_array($algorithmName, AlgorithmInterface::ALGORITHM_LIST) === false) {
            throw new Exception('Invalid algorithm name');
        }

        $this->algorithmName = $algorithmName;

        return $this;
    }

    public function getAlgorithmName(): string
    {
        return $this->algorithmName;
    }
}
