<?php

namespace App\Application\Command\Binance\Signature;

use App\Application\Command\AbstractCommand;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     example={
 *         "params": "recvWindow=60000&symbol=XRPUSDT"
 *     }
 * )
 */
class SignatureCommand extends AbstractCommand
{
    protected string $params;

    public function getParams(): string
    {
        return $this->params;
    }
}
