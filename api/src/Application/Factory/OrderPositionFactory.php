<?php

namespace App\Application\Factory;

use App\Domain\Contract\Algorithm\AlgorithmInterface;
use App\Domain\Contract\Factory\OrderPositionFactoryInterface;
use App\Domain\Contract\Repository\MySQL\FuturesCoinLeverageRepositoryInterface;
use App\Domain\Entity\Coin;
use App\Domain\Entity\DTO\CoinPriceChange;
use App\Domain\Entity\DTO\TrendChange;
use App\Domain\Entity\OrderPosition;
use DateTime;

class OrderPositionFactory implements OrderPositionFactoryInterface
{
    public function __construct(
        private FuturesCoinLeverageRepositoryInterface $coinLeverageRepository,
    ) {
    }

    public function getForCoinPriceChangeAlgorithm(CoinPriceChange $coinPriceChange): OrderPosition
    {
        return $this->getInstance()
            ->setCoin($coinPriceChange->getCoin())
            ->setDate(new DateTime())
            ->setType($coinPriceChange->getOrderType())
            ->setMarketPrice($coinPriceChange->getMarketPrice())
            ->setLeverage($this->coinLeverageRepository->find($coinPriceChange->getCoin()->getPairName()) ?? 0)
            ->setProfitPercent(OrderPosition::PERCENT_TO_PROFIT)
            ->setAlgorithmName(AlgorithmInterface::COIN_PRICE_SPOT_ALGORITHM);
    }

    public function getForTrendChangeAlgorithm(TrendChange $trendChange, Coin $coin): OrderPosition
    {
        return $this->getInstance()
            ->setCoin($coin)
            ->setDate(new DateTime())
            ->setType($trendChange->getTrend())
            ->setMarketPrice($trendChange->getPrice())
            ->setLeverage(1)
            ->setProfitPercent(0)
            ->setAlgorithmName(AlgorithmInterface::TREND_CHANGE_ALGORITHM);
    }

    public function getInstance(): OrderPosition
    {
        return new OrderPosition();
    }
}
