<?php

namespace App\Application\Factory;

use App\Domain\Contract\Factory\CoinPriceFactoryInterface;
use App\Domain\Entity\Coin;
use App\Domain\Entity\CoinPrice;
use DateTime;

class CoinPriceFactory implements CoinPriceFactoryInterface
{
    public function getInstance(): CoinPrice
    {
        return new CoinPrice();
    }

    public function getSpotCoinPrice(
        Coin $coin,
        float $marketPrice
    ): CoinPrice {
        return $this->getInstance()
            ->setCoin($coin)
            ->setMarketPrice($marketPrice)
            ->setDate(new DateTime());
    }
}
