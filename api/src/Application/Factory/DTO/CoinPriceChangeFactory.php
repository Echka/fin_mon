<?php

namespace App\Application\Factory\DTO;

use App\Domain\Entity\CoinPrice;
use App\Domain\Entity\DTO\CoinPriceChange;

class CoinPriceChangeFactory
{
    public function getCoinPriceChange(
        CoinPrice $startPrice,
        CoinPrice $finishPrice,
        int $needPercent,
    ): CoinPriceChange {
        return (new CoinPriceChange())
            ->setStartPrice($startPrice)
            ->setFinishPrice($finishPrice)
            ->setNeedPercent($needPercent)
            ->calcChangePercent()
            ->setCoin($startPrice->getCoin());
    }
}
