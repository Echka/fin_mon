<?php

namespace App\Application\Factory;

use App\Application\Command\Setting\Update\UpdateCommand;
use App\Application\Service\UserService;
use App\Domain\Contract\Algorithm\AlgorithmInterface;
use App\Domain\Contract\Factory\SettingFactoryInterface;
use App\Domain\Entity\Setting;

class SettingFactory implements SettingFactoryInterface
{
    public function __construct(
        private UserService $userService,
    ) {
    }

    public function getInstance(): Setting
    {
        return new Setting();
    }

    public function fillEntity(Setting $entity, UpdateCommand $command): Setting
    {
        return $entity
            ->setBinancePublicKey($command->getBinancePublicKey())
            ->setBinancePrivateKey($command->getBinancePrivateKey())
            ->setMonoBankToken($command->getMonoBankToken())
            ->setUser($this->userService->getCurrentUser())
            ->setAlgorithmSettings(
                $this->getClearAlgorithmSettings($command->getAlgorithmSettings())
            );
    }

    private function getClearAlgorithmSettings(array $algorithmSettings): array
    {
        $clearAlgorithmSettings = [];

        foreach ($algorithmSettings as $algorithmName => $algorithmFields) {
            if (in_array($algorithmName, AlgorithmInterface::ALGORITHM_LIST) === false) {
                continue;
            }

            $clearAlgorithmSettings[$algorithmName] = AlgorithmInterface::ALGORITHM_DEFAULT_SETTING;

            $this->setClearAlgorithmSettingFields(
                clearAlgorithmSetting: $clearAlgorithmSettings[$algorithmName],
                algorithmFields: $algorithmFields,
            );
        }

        return $clearAlgorithmSettings;
    }

    private function setClearAlgorithmSettingFields(array &$clearAlgorithmSetting, array $algorithmFields): void
    {
        foreach ($algorithmFields as $field => $value) {
            if (in_array($field, AlgorithmInterface::ALGORITHM_SETTING_FIELDS) === false) {
                continue;
            }

            $clearAlgorithmSetting[$field] = $value;
        }
    }
}
