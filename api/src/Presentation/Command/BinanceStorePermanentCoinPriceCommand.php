<?php

namespace App\Presentation\Command;

use App\Domain\Contract\Factory\CoinPriceFactoryInterface;
use App\Domain\Contract\Repository\MySQL\CoinPriceRepositoryInterface;
use App\Domain\Contract\Repository\MySQL\CoinRepositoryInterface;
use App\Domain\Contract\Repository\Redis\SpotCoinPriceRepositoryInterface;
use App\Domain\Entity\Coin;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BinanceStorePermanentCoinPriceCommand extends Command
{
    public function __construct(
        private readonly CoinRepositoryInterface $coinRepository,
        private readonly SpotCoinPriceRepositoryInterface $spotCoinPriceRepository,
        private readonly CoinPriceRepositoryInterface $coinPriceRepository,
        private readonly CoinPriceFactoryInterface $coinPriceFactory,
    ) {
        parent::__construct();
    }

    public static function getDefaultName(): string
    {
        return 'binance:store:permanent:coin_price';
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->storeSpotCoinPrice();

        return self::SUCCESS;
    }

    private function storeSpotCoinPrice(): void
    {
        $coins = $this->coinRepository->all();

        /** @var Coin $coin */
        foreach ($coins as $coin) {
            try {
                $coinPrice = $this->coinPriceFactory->getSpotCoinPrice(
                    $coin,
                    $this->spotCoinPriceRepository->find($coin->getPairName()),
                );

                $this->coinPriceRepository->store($coinPrice, false);
            } catch (Exception) {
            }
        }

        $this->coinPriceRepository->flush();
    }
}
