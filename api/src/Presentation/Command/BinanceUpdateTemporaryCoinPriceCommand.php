<?php

namespace App\Presentation\Command;

use App\Application\Request\Binance\CoinPriceRequest;
use App\Domain\Contract\Repository\MySQL\CoinRepositoryInterface;
use App\Domain\Contract\Repository\Redis\SpotCoinPriceRepositoryInterface;
use App\Domain\Entity\Coin;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BinanceUpdateTemporaryCoinPriceCommand extends Command
{
    public function __construct(
        private readonly CoinRepositoryInterface $coinRepository,
        private readonly SpotCoinPriceRepositoryInterface $spotCoinPriceRepository,
        private readonly CoinPriceRequest $coinPriceRequest,
    ) {
        parent::__construct();
    }

    public static function getDefaultName(): string
    {
        return 'binance:update:temporary:coin_price';
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $coinPrices = $this->coinPriceRequest->sendRequest(
            array_map(
                fn(Coin $coin) => $coin->getPairName(),
                $this->coinRepository->all()
            )
        );

        foreach ($coinPrices as $coinPrice) {
            $this->spotCoinPriceRepository->save(
                $coinPrice['symbol'],
                $coinPrice['price'],
            );
        }

        return self::SUCCESS;
    }
}
