<?php

namespace App\Presentation\Command;

use App\Application\Command\CheckByAlgorithm\CheckByAlgorithmCommand;
use App\Domain\Contract\Algorithm\AlgorithmInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class RunMainCryptoTrendAlgorithmCommand extends Command
{
    public function __construct(
        private MessageBusInterface $messageBus,
    ) {
        parent::__construct();
    }

    public static function getDefaultName(): string
    {
        return 'algorithm_check:main_crypto_trend';
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->messageBus->dispatch(
            (new CheckByAlgorithmCommand())
                ->setAlgorithmName(AlgorithmInterface::MAIN_CRYPTO_TREND_ALGORITHM)
        );

        return self::SUCCESS;
    }
}
