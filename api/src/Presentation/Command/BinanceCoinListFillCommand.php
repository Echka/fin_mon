<?php

namespace App\Presentation\Command;

use App\Application\Service\CoinService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class BinanceCoinListFillCommand extends Command
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private CoinService $coinService,
    ) {
        parent::__construct();
    }

    public static function getDefaultName(): string
    {
        return 'binance:coin:list:fill';
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->coinService->fillCoinsFromBinance();

        return self::SUCCESS;
    }
}
