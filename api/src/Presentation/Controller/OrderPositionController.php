<?php

namespace App\Presentation\Controller;

use App\Application\Command\OrderPosition\List\ListCommand;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class OrderPositionController extends AbstractController
{
    /**
     * @Route("/v1/order_position", name="order_position_list", methods={"GET"})
     * @OA\Get(summary="List of Order Position")
     * @OA\Response(response=Response::HTTP_OK, description="OK")
     * @OA\RequestBody(
     *     @OA\MediaType(mediaType="application/json",
     *          @OA\Schema(ref=@Model(type=ListCommand::class))
     *     )
     * )
     * @Security(name="Bearer")
     * @throws ExceptionInterface
     */
    public function list(): Response
    {
        return $this->response($this->handle(ListCommand::class));
    }
}
